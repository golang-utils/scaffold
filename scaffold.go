package scaffold

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/user"
	"regexp"
	"strings"
	"text/template"
	"time"

	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/updatechecker"
)

func init() {
	updatechecker.PrintVersionCheck("gitlab.com/golang-utils/scaffold")
}

// FuncMap provides functions to the template.
// New functions can be added as needed. The usual restrictions for
// text/template.FuncMap apply (see http://golang.org/pkg/text/template/#FuncMap)
var FuncMap = template.FuncMap{
	"user":             User,
	"args":             Args,
	"now":              Now,
	"host":             Host,
	"replace":          Replace,
	"filename":         FileName,
	"filenameLower":    FileNameLower,
	"camelCase1":       CamelCase1,
	"camelCase2":       CamelCase2,
	"title":            strings.Title,
	"toLower":          strings.ToLower,
	"toUpper":          strings.ToUpper,
	"trim":             strings.Trim,
	"doubleCurlyOpen":  DoubleCurlyOpen,
	"doubleCurlyClose": DoubleCurlyClose,
	"curlyOpen":        CurlyOpen,
	"curlyClose":       CurlyClose,
	"dollar":           Dollar,
}

func User() string {
	u, err := user.Current()
	if err != nil {
		return ""
	}

	return u.Name
}

func Now() string {
	return fmtdate.FormatDateTime(time.Now())
}

func Host() string {
	host, err := os.Hostname()
	if err != nil {
		return ""
	}
	return host
}

func Args() string {
	return strings.Join(os.Args, " ")
}

// Dollar returns a dollar char
func Dollar() string {
	return "$"
}

// CurlyOpen returns one open curly brace
func CurlyOpen() string {
	return "{"
}

// CurlyClose returns one closed curly brace
func CurlyClose() string {
	return "}"
}

// DoubleCurlyOpen returns two open curly braces
func DoubleCurlyOpen() string {
	return "{{"
}

// DoubleCurlyClose returns two closed curly braces
func DoubleCurlyClose() string {
	return "}}"
}

var fnameRegExp = regexp.MustCompile("([^-a-zA-Z_0-9])")
var fnameRegExp2nd = regexp.MustCompile("(-+)")

// FileName converts a string to a safe filename.
// First leading and trailing spaces are removed
// Every other space is replaced by -
// Multiple following - are replaced by on single -
// Every character other than [-a-zA-Z_0-9] is removed
func FileName(in string) (out string) {
	out = strings.TrimSpace(in)
	out = strings.Replace(out, " ", "-", -1)
	out = fnameRegExp.ReplaceAllLiteralString(out, "")
	out = fnameRegExp2nd.ReplaceAllLiteralString(out, "-")
	return
}

// same as FileName but transforms string to lowercase
func FileNameLower(src string) string {
	return strings.ToLower(src)
}

// CamelCase1 converts a string in snake_case to CamelCase where the first letter of each word is capitalized
func CamelCase1(src string) string {
	s := strings.Split(src, "_")
	for i := range s {
		s[i] = strings.Title(s[i])
	}
	return strings.Join(s, "")
}

// CamelCase2 converts a string in snake_case to camelCase where the first letter of each but the first word is capitalized
func CamelCase2(src string) string {
	s := strings.Split(src, "_")
	for i := range s {
		if i != 0 {
			s[i] = strings.Title(s[i])
		}
	}
	return strings.Join(s, "")
}

// Replace replaces every occurence of old in s by new
func Replace(s, old, new string) string {
	return strings.Replace(s, old, new, -1)
}

// writeFileFS creates the given file with the given content if isTest is true.
// Needed directories are created on the fly and each file name is written to log
// if log is not nil.
// If isTest is true, no files and directories are created.
//func writeFileFS(fsys fs.FS, file path.Relative, content []byte, log io.Writer) error {
//	return fsys.Write(file, fs.ReadCloser(bytes.NewReader(content)), true)
//}

//func ensureDirFS(fsys fs.FS, dir path.Relative) error {
//	return fs.MkDirAll(fsys, dir)
//}

type dirStack struct {
	dirs []path.Relative
}

func (d *dirStack) push(r path.Relative) {
	d.dirs = append(d.dirs, r)
}

func (d *dirStack) last() path.Relative {
	if len(d.dirs) == 0 {
		return path.Relative("")
	}
	return d.dirs[len(d.dirs)-1]
}

func (d *dirStack) pop() path.Relative {
	rel := d.last()
	d.dirs = d.dirs[:len(d.dirs)-1]
	return rel
}

func parseGeneratorFS(fsys fs.FS, rd io.Reader, log io.Writer) error {
	var dirs dirStack
	scanner := bufio.NewScanner(rd)
	var file path.Relative
	var dir = path.Relative("")
	var bf bytes.Buffer
	var line = -1
	for scanner.Scan() {
		line++
		s := scanner.Text()
		if strings.HasPrefix(s, ">>>") {
			fd := strings.TrimSpace(strings.TrimPrefix(s, ">>>"))
			if fd[len(fd)-1] == '/' {
				//dir = filepath.ToSlash(filepath.Join(dir, fd))
				//fmt.Printf("dir is %v\n", dir)
				dir = dir.Join(fd)
				err := fs.MkDirAll(fsys, dir)
				if err != nil {
					return fmt.Errorf("could not create directory %q: %s\n", dir, err)
				}
				if log != nil {
					log.Write([]byte(dir + "\n"))
				}
				dirs.push(dir)
				file = ""
			} else {
				bf.Reset()
				if file != "" {
					return fmt.Errorf("syntax error in line %d embedding file within file is not allowed (%#v inside %#v)", line, fd, file)
				}

				file = dir.Join(fd)
				if log != nil {
					log.Write([]byte(file + "\n"))
				}
			}
			continue
		}

		if strings.HasPrefix(s, "<<<") {
			fd := strings.TrimSpace(strings.TrimPrefix(s, "<<<"))
			if fd[len(fd)-1] == '/' {
				dir = dirs.pop()
				if path.Base(dir) != path.Base(path.Relative(fd)) {
					return fmt.Errorf("syntax error in line %d closing dir %#v but should close dir %#v", line, path.Base(path.Relative(fd)), path.Base(dir))
				}
				dir = dirs.last()
			} else {
				if path.Base(file) != path.Base(path.Relative(fd)) {
					return fmt.Errorf("syntax error in line %d closing file %#v but should close file %#v", line, path.Base(path.Relative(fd)), path.Base(file))
				}
				bt := bf.Bytes()
				if len(bt) > 1 {
					bt = bt[1:]
				}
				err := fsys.Write(file, fs.ReadCloser(bytes.NewReader(bt)), true)
				if err != nil {
					return err
				}
				file = ""
				bf.Reset()
			}
			continue
		}

		bf.WriteString("\n" + s)
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	return nil
}

// SplitTemplate splits the given template on the first empty line.
// It returns the head and body of the template.
// Templates must be UTF8 without byte order marker and have \n (linefeed) as line terminator.
func SplitTemplate(template string) (head, body string) {
	spl := strings.SplitN(template, "\n\n", 2)
	if len(spl) != 2 {
		return "", template
	}
	return spl[0], spl[1]
}

// convertJSON converts json to a map
func convertJSON(rd io.Reader) (data map[string]interface{}, err error) {
	data = map[string]interface{}{}
	err = json.NewDecoder(rd).Decode(&data)
	return
}

// mix mixes the given data to the template body
func mix(body string, data map[string]interface{}) (rd io.Reader, err error) {
	var bf bytes.Buffer

	t := template.New("x").Funcs(FuncMap)
	t, err = t.Parse(body)
	if err != nil {
		return
	}
	err = t.Execute(&bf, data)
	rd = &bf
	return
}

// RunFS is
func RunFS(fsys fs.FS, body string, json io.Reader, log io.Writer) (err error) {
	placeholders := map[string]interface{}{}

	if json != nil {
		placeholders, err = convertJSON(json)
		if err != nil {
			return err
		}
	}

	generator, err := mix(body, placeholders)

	if err != nil {
		return err
	}

	return parseGeneratorFS(fsys, generator, log)
}

// Run mixes the properties of the json object to the template body. The result is then used
// to create files and directories beneath baseDir.
// If log is not nil a list of files that will be created is written to log.
func Run(baseDir path.Local, body string, json io.Reader, log io.Writer) (err error) {
	fsys, err := dirfs.New(baseDir)

	if err != nil {
		return err
	}
	return RunFS(fsys, body, json, log)
}
