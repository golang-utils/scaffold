module gitlab.com/golang-utils/scaffold

go 1.23.0

require (
	gitlab.com/golang-utils/fmtdate v1.0.2
	gitlab.com/golang-utils/fs v0.20.0
	gitlab.com/golang-utils/updatechecker v0.0.9
)

require (
	github.com/moby/sys/mountinfo v0.7.1 // indirect
	gitlab.com/golang-utils/errors v0.0.2 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
