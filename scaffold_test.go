package scaffold

import (
	"bytes"
	"sort"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

/*
func TestSplitFilename(t *testing.T) {

	tests := []struct {
		input string
		bare  string
		ext   string
	}{
		{"test.txt", "test", ".txt"},
		{"test.a.b", "test.a", ".b"},
	}

	for _, test := range tests {
		bare, ext := splitFilename(test.input)

		if bare != test.bare || ext != test.ext {
			t.Errorf("splitFilename(%#v) = %#v, %#v; want %#v, %#v", test.input, bare, ext, test.bare, test.ext)
		}
	}

}
*/
/*
func TestIsLowercase(t *testing.T) {

	tests := []struct {
		input    string
		expected bool
	}{
		{"A", false},
		{"B", false},
		{"C", false},
		{"D", false},
		{"X", false},
		{"Y", false},
		{"Z", false},
		{"a", true},
		{"b", true},
		{"c", true},
		{"d", true},
		{"x", true},
		{"y", true},
		{"z", true},
	}

	for _, test := range tests {

		if got, want := isLowercase(test.input), test.expected; got != want {
			t.Errorf("isLowercase(%#v) = %v; want %v", test.input, got, want)
		}
	}

}
*/

func TestFileName(t *testing.T) {

	/*
		([^-a-zA-Z_0-9])
	*/

	tests := []struct {
		input, expected string
	}{
		{" \\A/ want's 853 : until . # * +-0?@µ ", "A-wants-853-until-0"},
	}

	for _, test := range tests {

		if got, want := FileName(test.input), test.expected; got != want {
			t.Errorf("FileName(%#v) = %#v; want %#v", test.input, got, want)
		}
	}

}

func TestCamelCase1(t *testing.T) {

	tests := []struct {
		input, expected string
	}{
		{"field_name", "FieldName"},
		{"FieldName", "FieldName"},
		{"Field_name", "FieldName"},
		{"field_Name", "FieldName"},
		{"fieldname", "Fieldname"},
	}

	for _, test := range tests {

		if got, want := CamelCase1(test.input), test.expected; got != want {
			t.Errorf("CamelCase1(%v) = %v; want %v", test.input, got, want)
		}
	}

}

func TestCamelCase2(t *testing.T) {

	tests := []struct {
		input, expected string
	}{
		{"field_name", "fieldName"},
		{"FieldName", "FieldName"},
		{"Field_name", "FieldName"},
		{"field_Name", "fieldName"},
		{"fieldname", "fieldname"},
	}

	for _, test := range tests {

		if got, want := CamelCase2(test.input), test.expected; got != want {
			t.Errorf("CamelCase2(%#v) = %#v; want %#v", test.input, got, want)
		}
	}

}

func TestReplace(t *testing.T) {

	tests := []struct {
		input, src, dest, expected string
	}{
		{"field_name", "_", "*", "field*name"},
		{"field_name_x", "_", "*", "field*name*x"},
		{"_field_name_", "_", "*", "*field*name*"},
	}

	for _, test := range tests {

		if got, want := Replace(test.input, test.src, test.dest), test.expected; got != want {
			t.Errorf("Replace(%#v, %#v, %#v) = %#v; want %#v", test.input, test.src, test.dest, got, want)
		}
	}

}

var validHead = `{
	"Models": [
		{
			"Name": "",
			"Fields": [
				{"Name": "", "Type": ""}
			]
		}
	]
}`

var validBody = `
{{range .Models}}
>>>models/{{toLower .Name}}/
>>>model.go
package {{replace .Name "_" "."}}

type {{camelCase1 .Name}} struct {{curlyOpen}}
{{range .Fields}}	{{camelCase1 .Name}} {{.Type}}
{{end}}{{curlyClose}}

<<<model.go
<<<models/{{toLower .Name}}/
{{end}}
`

var validJSON = `
{
	"Models": [
		{
			"Name": "address",
			"Fields": [
				{
					"Name": "street_no",
					"Type": "string"
				},
				{
					"Name": "city",
					"Type": "string"
				}
			]
		},	
		{
			"Name": "person",
			"Fields": [
				{
					"Name": "first_name",
					"Type": "string"
				},
				{
					"Name": "last_name",
					"Type": "string"
				}
			]
		}
	]
}
`

var validTemplate = validHead + "\n\n" + validBody

func TestSplitTemplate(t *testing.T) {
	h, b := SplitTemplate(validTemplate)

	if h != validHead {
		t.Errorf("wrong head: %#v, expecting %#v", h, validHead)
	}

	if b != validBody {
		t.Errorf("wrong body: %#v, expecting %#v", b, validBody)
	}
}

func TestRun(t *testing.T) {
	//t.Skip()
	wd := path.MustWD()

	tests := []struct {
		body, json string
		files      map[string]string
	}{
		{
			">>>file.txt\n\n\n<<<file.txt",
			`{}`,
			map[string]string{
				"file.txt": "\n",
			},
		},

		{
			">>>file1.txt\nfile1\nfile1b\n<<<file1.txt\n>>>file2.txt\nfile2\nfile2b\n<<<file2.txt",
			`{}`,
			map[string]string{
				"file1.txt": "file1\nfile1b",
				"file2.txt": "file2\nfile2b",
			},
		},

		{
			">>>a/\n>>>b/\n>>>file1.txt\n<<<file1.txt\n>>>file2.txt\n<<<file2.txt\n<<<b/\n<<<a/\n",
			`{}`,
			map[string]string{
				"a/":            "b/",
				"a/b/":          "file1.txt\nfile2.txt",
				"a/b/file1.txt": "",
				"a/b/file2.txt": "",
			},
		},

		{
			"{{range .Files}}>>>{{.Name}}.txt\n{{.Content}}\n<<<{{.Name}}.txt\n{{end}}",
			`{"Files": [{"Name": "a/dir/file1", "Content": "content_file1"},{"Name": "a/dir/file2", "Content": "content_file2"}]}`,
			map[string]string{
				"a/dir/file1.txt": "content_file1",
				"a/dir/file2.txt": "content_file2",
			},
		},
		{
			validBody,
			validJSON,
			map[string]string{
				"models/address/": "model.go",
				"models/person/":  "model.go",
				"models/person/model.go": `package person

type Person struct {
	FirstName string
	LastName string
}
`,
				"models/address/model.go": `package address

type Address struct {
	StreetNo string
	City string
}
`,
			},
		},
	}

	fsys, err := mockfs.New(wd)

	if err != nil {
		t.Errorf("can't create mockfs: %v", err)
		return
	}

	for i, test := range tests {

		fsys.Clear()

		var log bytes.Buffer
		err = RunFS(fsys, test.body, strings.NewReader(test.json), &log)
		if err != nil {
			t.Errorf("[%v] RunFS(fsys, %#v, %#v,...) returned error: %v", i, test.body, test.json, err)
			continue
		}

		var files []string

		for k := range test.files {
			files = append(files, strings.TrimSpace(k))
		}

		sort.Strings(files)

		want := strings.Join(files, "\n")
		got := strings.TrimSpace(strings.Replace(log.String(), "\\", "/", -1))

		if got != want {
			t.Errorf("[%v] RunFS(fsys, %#v, %#v,...) = %#v; want %#v", i, test.body, test.json, got, want)
		}

		for file, content := range test.files {
			p := path.Relative(file)
			if path.IsDir(p) {
				entries, err := fs.ReadDirNames(fsys, p)
				if err != nil {
					t.Errorf("[%v] can't read dir %q", i, p)
				} else {
					var entr []string

					for _, rel := range entries {
						entr = append(entr, rel)
					}

					sort.Strings(entr)
					got := strings.Join(entr, "\n")
					want := content

					if got != want {
						t.Errorf("[%v] RunFS(fsys, %#v, %#v,...); dir %q contains %q but should contain %q", i, test.body, test.json, file, got, want)
					}
				}
			} else {
				cnt, err := fs.ReadFile(fsys, p)
				if err != nil {
					t.Errorf("[%v] RunFS(fsys, %#v, %#v,...); file %q could not be read: %v", i, test.body, test.json, file, err)
				}

				got := string(cnt)
				want := content

				if got != want {
					t.Errorf("[%v] RunFS(fsys, %#v, %#v,...); file %q contains %q but should contain %q", i, test.body, test.json, file, got, want)
				}
			}
		}
	}
}

func TestRunErrors(t *testing.T) {
	tests := []struct {
		body, json string
	}{
		{
			">>>file.txt\n<<<file.txt",
			`{`,
		},
		{
			">>file.txt\n<<<file.txt",
			`{}`,
		},
		{
			">>>file1.txt\n<<<file2.txt",
			`{}`,
		},
		{
			">>>a/\n>>>b\n>>>file1.txt\n<<<file1.txt\n<<<a/\n<<<b\n",
			`{}`,
		},
		{
			">>>file1.txt\nho\n>>>file2.txt\nhu<<<file2.txt\n<<<file1.txt\n",
			`{}`,
		},
		{
			"{{range .x}}",
			`{}`,
		},
	}

	fsys, err := mockfs.New(path.MustWD())

	if err != nil {
		t.Errorf("can't initialize mockfs: %v", err)
	}

	for _, test := range tests {
		var log bytes.Buffer
		err = RunFS(fsys, test.body, strings.NewReader(test.json), &log)
		if err == nil {
			t.Errorf("RunFS(fsys, %#v, %#v,...) returned no error", test.body, test.json)
		}
	}
}
