package runner

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/filesystems/rootfs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/scaffold"
)

type ErrTemplateNotFound string

func (n ErrTemplateNotFound) Error() string {
	return fmt.Sprintf("could not find template file %#v", string(n))
}

func findInDir(path, file string, verbose bool) bool {
	if verbose {
		println("looking for ", filepath.Join(path, file))
	}
	fullPath, err := filepath.Abs(filepath.Join(path, file))
	if err != nil {
		return false
	}

	var info os.FileInfo
	info, err = os.Stat(fullPath)

	if err != nil {
		return false
	}

	return !info.IsDir()
}

// findTemplateFile finds the file inside the given path and returns the found file or an error
func findTemplateFile(templatePath string, templateFile string, verbose bool) (fullPath string, err error) {
	paths := append([]string{""}, strings.Split(templatePath, ":")...)

	file := templateFile

	for _, p := range paths {
		if findInDir(p, file, verbose) {
			return filepath.Join(p, file), nil
		}
		if findInDir(p, file+".template", verbose) {
			return filepath.Join(p, file+".template"), nil
		}
	}
	return "", ErrTemplateNotFound(file)
}

func getTemplate(templatePath string, templateFile string, verbose bool) (head string, template string, err error) {
	file, err := findTemplateFile(templatePath, templateFile, verbose)
	if err != nil {
		return "", "", err
	}
	fmt.Fprintln(os.Stdout, "found ", file)
	templateRaw, err := os.ReadFile(file)

	if err != nil {
		return "", "", err
	}

	head, template = scaffold.SplitTemplate(string(templateRaw))
	return
}

func Head(templatePath string, templateFile string, verbose bool) error {
	head, _, err := getTemplate(templatePath, templateFile, verbose)

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, head)
	return nil
}

// func Scan(fsys fs.ReadOnly, scanDir path.Relative) error {
func Scan(dir string) error {

	d, err := path.ParseDirFromSystem(dir)

	if err != nil {
		return err
	}

	fsys, err := rootfs.New()

	if err != nil {
		return err
	}

	r := d.RootRelative()

	if !fsys.Exists(r) {
		return fs.ErrNotFound.Params(dir)
	}

	templ, err := scaffold.Scan(fsys, r)

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, string(templ))
	return nil
}

type ScaffoldConfig struct {
	TemplatePath string
	TemplateFile string
	Verbose      bool
	Dir          string
	IsTest       bool
	DataFile     string
}

func Scaffold(conf ScaffoldConfig) error {
	dir, err := filepath.Abs(conf.Dir)

	if err != nil {
		return err
	}

	_, template, err := getTemplate(conf.TemplatePath, conf.TemplateFile, conf.Verbose)

	if err != nil {
		return err
	}

	var rd io.Reader

	switch conf.DataFile {
	case "[STDIN]":
		rd = os.Stdin
	case "":
		rd = nil
	default:
		fl, err := os.Open(conf.DataFile)

		if err != nil {
			return err
		}

		defer fl.Close()
		rd = fl
	}

	if !strings.HasSuffix(dir, "/") {
		dir += "/"
	}

	loc, err := path.ParseLocal(dir)

	if err != nil {
		return err
	}

	var fsys fs.FS

	if conf.IsTest {
		fsys, err = mockfs.New(loc)
	} else {
		fsys, err = dirfs.New(loc)
	}

	if err != nil {
		return err
	}

	return scaffold.RunFS(fsys, template, rd, os.Stdout)
}

func Templates(templatePath string) error {
	paths := strings.Split(templatePath, ":")
	for _, path := range paths {
		if path == "" {
			continue
		}
		fileinfos, err := os.ReadDir(path)
		if err != nil {
			if os.IsNotExist(err) {
				fmt.Fprintf(os.Stderr, "skipping %s (missing)\n", path)
			} else {
				fmt.Fprintf(os.Stderr, "skipping %s (%s)\n", path, err)
			}
		} else {
			var bf bytes.Buffer
			for _, fi := range fileinfos {
				if !fi.IsDir() {
					name := fi.Name()
					if name[0] != '.' {
						bf.WriteString("  " + name + "\n")
						// fmt.Fprintln(os.Stdout, name)
					}
				}
			}

			if bf.String() == "" {
				fmt.Fprintf(os.Stdout, "no templates inside %s\n", path)
			} else {
				fmt.Fprintf(os.Stdout, "templates inside %s:\n%s\n", path, bf.String())
			}
		}
	}
	return nil
}
