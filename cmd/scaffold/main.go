package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/golang-utils/scaffold/lib/runner"
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

var cfg = config.New("scaffold",
	"scaffold creates files and directories based on a template and json input.",
	config.Copyright("(C) by Marc Rene Arns"),
	config.Authors("Marc Rene Arns"),
	config.Documentation(`docs for the template functions and for the library:
https://pkg.go.dev/gitlab.com/golang-utils/scaffold
`),
	config.Bugs(`There are some bugs in the scan command (empty lines, spaces etc.)`),
	config.ReportingBugs(`Please report bugs to https://gitlab.com/golang-utils/scaffold`),
	config.Concept(concept),
	config.License("MIT"),
	config.Example(example),
)

var concept = `The basic idea is to extend the power of the Go template engine to create text files and directories, i.e. have one template file generate a whole directory structure with source code files or configurations.
Therefor placeholders are used that can be filled with json objects when the template is "executed", i.e. the files and folders are written to the disk.
Since everything is within a single template file, we need a way to tell, when we enter or leave a directory or a file.
The prefix >>> at the beginning of a line means "entering" and <<< at the beginning of a line means "leaving".
After these prefixes follows the name of a file or directory. If the name ends with a slash / it is considered a directory, otherwise a file.
We can use placeholders, iterations and conditions everywhere, because the Go template engine is called first,  before the result is split into files and folders.
This way it is also possible to iterate over json arrays constructing several files and folders of a similar type with different details (see the Example).
For a detailed description of the features of the Go template engine, see:
  https://pkg.go.dev/text/template
For the set of added helper functions, see:
  https://pkg.go.dev/gitlab.com/golang-utils/scaffold
There is also a way to create a template file from an existing directory structure (see the 'scan' command). Then placeholders inside file and directory names must start with # in order to be identified.
Although it is not strictly necessary, it is a good practise to write an example of the expected json structure at the beginning of the template. It should not contain any empty line.
The first empty line tells the parser, that the real templates starts with the next non empty line.
The example json can easily be shown with the help of the 'head' command.
`

var example = `
given the following template (file models.templ)
  {
    "Models": [
	    {"Name": "",
       "Fields": [ {"Name": "", "Type": ""} ] }
    ]
  }

  {{range .Models}}
  >>>models/{{toLower .Name}}/
  >>>model.go
  package {{replace .Name "_" "."}}

  type {{camelCase1 .Name}} struct {{curlyOpen}}
  {{range .Fields}}	{{camelCase1 .Name}} {{.Type}}
  {{end}}{{curlyClose}}
  <<<model.go
  <<<models/{{toLower .Name}}/
  {{end}}

and the following json (file models.json)
  {"Models": [
    {"Name": "person",
     "Fields": [
       {"Name": "first_name","Type": "string"},
       {"Name": "last_name" ,"Type": "string"}
     ]
    },
    {"Name": "address",
     "Fields": [
       {"Name": "street_no","Type": "string"},
       {"Name": "city","Type": "string"}
     ]
    ]
  }

when running the command
  scaffold -t=models.templ < models.json
the following directory structure would be build:
  models
    address
      model.go
    person
      model.go

where models/address/model.go contains
  package address

  type Address struct {
    StreetNo string
    City string
  }

and models/person/model.go contains
  package person

  type Person struct {
    FirstName string
    LastName string
  }
`

var (
	// general arguments / main command

	paramTemplateFile = cfg.String("template",
		"the file where the template resides",
		config.Default("scaffold.template"),
		config.Shortflag('t'),
	)

	paramDataFile = cfg.String("data",
		"the file in JSON format that will be fed into the template. [STDIN] signifies that the data is "+
			"coming from stdin. Empty string signifies that the template will be used without data.",
		//config.Default("[STDIN]"),
		config.Shortflag('d'),
	)

	paramTargetDir = cfg.String("dir",
		"directory that is the target/root of the file creations",
		config.Default("."),
	)

	paramTemplatesPath = cfg.String("path",
		"the path to look for template files, the directories must be separated by a colon (:)",
	)

	paramVerbose = cfg.Bool("verbose",
		"show verbose messages",
		config.Default(false),
		config.Shortflag('v'),
	)
)

var cmdShowHead = cfg.Command("head",
	"shows the head section of the given template").
	Skip("dir").Skip("data")

var cmdTestRun = cfg.Command("test",
	"makes a test run without creating any files")

var (
	// scan command
	cmdScanDir = cfg.Command("scan",
		"scan scans a directory and generates a template based on it. "+
			"placeholders in dirs and files must start with #", config.Example(`
given this folder structure
  template_folder/
    #company/
      #company.html
      #other.txt

with #company.html containing
  <html>
	  <head>
	  {{.Head}}
	  </head>
	  <body>
	  {{.Body}}
	  </body>
  </html>

and #other.txt containing
  {{.Message}}

when running
  scaffold scan --scandir='template_folder' > my.templ

my.templ contains:
  >>>template_folder/
  >>>{{filenameLower .Company}}/
  >>>{{filenameLower .Company}}.html
  <html>
    <head>
    {{.Head}}
    </head>
    <body>
    {{.Body}}
    </body>
  </html>
  <<<{{filenameLower .Company}}.html
  >>>{{filenameLower .Other}}.txt
    {{.Message}}
  <<<{{filenameLower .Other}}.txt
  <<<{{filenameLower .Company}}/
  <<<template_folder/

			`)).
		Skip("template").Skip("dir").Skip("data").Skip("path")
	paramScanDirSource = cmdScanDir.String("scandir",
		"directory which is scanned to create the template",
		config.Default("."),
	)
)

var cmdShowTemplateList = cfg.Command("list",
	"prints a list of template files, residing in path").
	Skip("template").Skip("data")

func run() error {
	err := cfg.Run()

	if err != nil {
		fmt.Fprintln(os.Stdout, "\n\n--------------------------------------\n\n"+cfg.Usage())
		return err
	}

	switch cfg.ActiveCommand() {

	case cmdScanDir:
		return runner.Scan(paramScanDirSource.Get())

	case cmdShowTemplateList:
		return runner.Templates(paramTemplatesPath.Get())

	case cmdShowHead:
		return runner.Head(paramTemplatesPath.Get(), paramTemplateFile.Get(), paramVerbose.Get())

	case cmdTestRun:
		return runner.Scaffold(makeConfig(true))

	case nil:
		return runner.Scaffold(makeConfig(false))

	default:
		panic("unreachable")
	}
}

func makeConfig(isTest bool) runner.ScaffoldConfig {

	return runner.ScaffoldConfig{
		DataFile:     paramDataFile.Get(),
		Dir:          paramTargetDir.Get(),
		Verbose:      paramVerbose.Get(),
		TemplateFile: paramTemplateFile.Get(),
		TemplatePath: paramTemplatesPath.Get(),
		IsTest:       isTest,
	}
}
