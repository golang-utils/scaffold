package scaffold

import (
	"bytes"
	"fmt"
	"regexp"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

type scannerNew struct {
	skipDirRegex *regexp.Regexp
	fsys         fs.ReadOnly
	bf           bytes.Buffer
}

func (s *scannerNew) scanFile(file path.Relative) error {
	fmt.Fprint(&s.bf, "\n>>>"+path.Base(file)+"\n")

	defer func() {
		fmt.Fprint(&s.bf, "\n"+"<<<"+path.Base(file))
	}()

	bt, err := fs.ReadFile(s.fsys, file)

	if err != nil {
		return err
	}

	s.bf.Write(bt)

	return nil
}

func (s *scannerNew) scanDir(dir path.Relative) error {
	entries, err := fs.ReadDirPaths(s.fsys, dir)

	if !dir.IsRoot() {
		if s.skipDirRegex != nil && s.skipDirRegex.MatchString(path.Name(dir)) {
			return nil
		}

		fmt.Fprint(&s.bf, "\n>>>"+path.Base(dir))

		defer func() {
			fmt.Fprint(&s.bf, "\n"+"<<<"+path.Base(dir))
		}()
	}

	if err != nil {
		return err
	}

	var dirs []path.Relative

	for _, entry := range entries {
		if path.IsDir(entry) {
			dirs = append(dirs, entry)
		} else {
			err = s.scanFile(entry)
			if err != nil {
				return err
			}
		}
	}

	for _, subdir := range dirs {
		err = s.scanDir(subdir)
		if err != nil {
			return err
		}
	}

	return nil
}

// scans a directory recursively
// and creates a template based on the structure of the files and directories
func Scan(fsys fs.ReadOnly, dir path.Relative, opts ...ScanOption) (template []byte, err error) {

	if !path.IsDir(dir) {
		return nil, fs.ErrExpectedDir.Params(dir)
	}

	s := &scannerNew{
		fsys: fsys,
	}

	for _, opt := range opts {
		opt(s)
	}

	err = s.scanDir(dir)

	if err != nil {
		return nil, err
	}

	return s.bf.Bytes(), nil
}

type ScanOption func(*scannerNew)

func SkipDirs(regexstring string) ScanOption {
	re := regexp.MustCompile(regexstring)
	return func(s *scannerNew) {
		s.skipDirRegex = re
	}
}
