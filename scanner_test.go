package scaffold

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

func TestScanner(t *testing.T) {

	fsys, err := mockfs.New(path.MustWD())

	if err != nil {
		t.Fatal(err.Error())
	}

	tests := []struct {
		files        map[path.Relative]string
		scanningDir  path.Relative
		skipDirRegex string
		expected     string
	}{
		{
			map[path.Relative]string{},
			"",
			"",
			"",
		},

		{
			map[path.Relative]string{
				"a.txt": "hiho",
			},
			"",
			"",
			"\n>>>a.txt\nhiho\n<<<a.txt",
		},

		{
			map[path.Relative]string{
				"b/a.txt": "hiho",
			},
			"",
			"",
			"\n>>>b/\n>>>a.txt\nhiho\n<<<a.txt\n<<<b/",
		},

		{
			map[path.Relative]string{
				"b/a.txt": "hiho",
				"c/a.txt": "hoho",
			},
			"",
			"",
			"\n>>>b/\n>>>a.txt\nhiho\n<<<a.txt\n<<<b/\n>>>c/\n>>>a.txt\nhoho\n<<<a.txt\n<<<c/",
		},

		{
			map[path.Relative]string{
				"c/b/a.txt": "hiho",
				"c/a.txt":   "hoho",
			},
			"",
			"",
			"\n>>>c/\n>>>a.txt\nhoho\n<<<a.txt\n>>>b/\n>>>a.txt\nhiho\n<<<a.txt\n<<<b/\n<<<c/",
		},

		{
			map[path.Relative]string{
				"c/b/a.txt": "hiho",
				"c/a.txt":   "hoho",
			},
			"",
			"b",
			"\n>>>c/\n>>>a.txt\nhoho\n<<<a.txt\n<<<c/",
		},

		{
			map[path.Relative]string{
				"c/b/a.txt": "hiho",
				"c/a.txt":   "hoho",
			},
			"",
			"c",
			"",
		},
	}

	for i, test := range tests {

		fsys.Clear()

		for file, content := range test.files {
			err = fs.WriteFile(fsys, file, []byte(content), true)
			if err != nil {
				t.Fatalf("[%v] could not create file %s: %v", i, file, err)
			}
		}

		var res []byte

		if test.skipDirRegex != "" {
			res, err = Scan(fsys, test.scanningDir, SkipDirs(test.skipDirRegex))
		} else {
			res, err = Scan(fsys, test.scanningDir)
		}

		if err != nil {
			t.Fatalf("[%v] Scan(fsys, %q, SkipDirs(%q) returned error %v", i, test.scanningDir, test.skipDirRegex, err)
		}

		got := string(res)
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Scan(fsys, %q, SkipDirs(%q) returned \n%s\n expected: \n%s\n", i, test.scanningDir, test.skipDirRegex, got, expected)
		}

	}

}
